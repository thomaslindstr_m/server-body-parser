# @amphibian/server-body-parser

[![build status](https://gitlab.com/thomaslindstr_m/server-body-parser/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/server-body-parser/commits/master)

parse incoming bodies for @amphibian/server

```
npm install @amphibian/server-body-parser
```

```javascript
var createServer = require('@amphibian/server');
var bodyParser = require('@amphibian/server-body-parser');
var server = createServer();

server.registerMiddleware(bodyParser);
server.registerHandler(function (context) {
    console.log(context.request.body);
});
```
