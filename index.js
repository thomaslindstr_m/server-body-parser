/* eslint-disable no-var */

var connect = require('koa-connect');
var parsers = require('body-parser');

var parse = connect(parsers.json());

module.exports = function bodyParser(context, next) {
    return parse(context, () => {
        context.request.body = context.req.body;
        return next();
    });
};
