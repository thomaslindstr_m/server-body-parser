var createServer = require('@amphibian/server').default;
var fetch = require('node-fetch');
var bodyParser = require('./index.js');
var assert = require('assert');

describe('server-body-parser', function() {
    it('can register the middleware', () => {
        var server = createServer({port: 4001, logging: false});
        server.registerMiddleware(bodyParser);
        server.close();
    });

    it('parses incoming bodies', (callback) => {
        var server = createServer({port: 4002, logging: false});

        server.registerMiddleware(bodyParser);
        server.registerHandler((context) => {
            assert.equal(context.request.body.hello, 'test');
            server.close();
            callback();
        });

        fetch('http://localhost:4002', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({hello: 'test'})
        });
    });
});
